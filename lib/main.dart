import 'package:flutter/material.dart';
import 'package:listview_and_provider/providers/listview_provider.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => ListViewProvider(),
      child: MaterialApp(
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ListViewProvider provider = Provider.of<ListViewProvider>(context);
    List<String> listOfStrings = provider.listOfString;
    return Scaffold(
      appBar: AppBar(
        title: Text('ListView and Provider'),
      ),
      body: Column(
        children: [
          Container(
            width: double.infinity,
            height: 250,
            child: ListView.builder(
              itemCount: listOfStrings.length,
              itemExtent: 100,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return Container(
                  margin: EdgeInsets.all(10),
                  color: Colors.blue,
                  child: Center(
                      child: Text(
                    listOfStrings[index],
                    style: TextStyle(
                      fontSize: 50,
                    ),
                  )),
                );
              },
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              ElevatedButton(
                onPressed: provider.decreaseList,
                child: Text('Decrease List'),
              ),
              ElevatedButton(
                onPressed: provider.increaseList,
                child: Text('Increase List'),
              ),
            ],
          )
        ],
      ),
    );
  }
}
