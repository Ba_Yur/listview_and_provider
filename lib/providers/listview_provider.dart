
import 'package:flutter/cupertino.dart';

class ListViewProvider with ChangeNotifier {

  List<String> _listOfString = ['1', '2'];

  List<String> get listOfString {
    return [..._listOfString];
  }

  void increaseList () {
    _listOfString.add((_listOfString.length +1).toString());
    notifyListeners();
  }

  void decreaseList () {
    _listOfString.removeLast();
    notifyListeners();
  }

}